#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=0

munge --help
let ret=$ret+$?

munge --license
let ret=$ret+$?

munge --version
let ret=$ret+$?

exit $ret
